#Kata del Juego de la vida de Conway

Realiza el ejercicio atendiendo al detalle, dando los pasos más pequeños que puedas cada vez y escribiendo un test que falle antes de la implementación.

## Descripción

El juego se desarrolla en un tablero bidimensional infinito. En cada coordenada habita una célula viva o muerta.

Cada vez que la rejilla evoluciona, todas las células mueren o renacen al mismo tiempo siguiendo las siguientes normas:

 - Una célula muerta con exactamente 3 células vecinas vivas renace.
 - Una célula viva con 2 ó 3 células vecinas vivas sigue viva.
 - En cualquier otro caso, una célula muere o permanece muerta (por "soledad" o "superpoblación").